//
//  TodayViewController.swift
//  NumberToday
//
//  Created by James Cash on 25-06-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

import UIKit
import NotificationCenter
import NumberLibrary

class TodayViewController: UIViewController, NCWidgetProviding {

    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var numberStepper: UIStepper!

    override func viewDidLoad() {
        super.viewDidLoad()
        let number = loadNumber()
        numberLabel.text = "\(number)"
        numberStepper.value = number
    }

    @IBAction func stepNumber(_ sender: UIStepper) {
        let number = sender.value
        store(number: number)
        numberLabel.text = "\(number)"
    }
        
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.
        let number = loadNumber()
        numberLabel.text = "\(number)"
        numberStepper.value = number

        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        
        completionHandler(NCUpdateResult.newData)
    }
    
}
