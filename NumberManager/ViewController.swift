//
//  ViewController.swift
//  NumberManager
//
//  Created by James Cash on 25-06-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

import UIKit
import NumberLibrary

class ViewController: UIViewController {

    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var numberSlider: UISlider!

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(forName: Notification.Name("reloadNumber"), object: DispatchQueue.main, queue: nil) { (_) in
            self.updateNumberDisplay()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNumberDisplay()
    }

    func updateNumberDisplay() {
        let number = loadNumber()
        numberLabel.text = "\(number)"
        numberSlider.value = Float(number)
    }

    @IBAction func numberChanged(_ sender: UISlider) {
        let number  = Double(sender.value)
        numberLabel.text = "\(number)"
        store(number: number)
    }
}
