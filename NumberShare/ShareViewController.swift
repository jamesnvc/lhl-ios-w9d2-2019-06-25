//
//  ShareViewController.swift
//  NumberShare
//
//  Created by James Cash on 25-06-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

import UIKit
import Social
import NumberLibrary

class ShareViewController: SLComposeServiceViewController {

    override func isContentValid() -> Bool {
        let nf = NumberFormatter()
        nf.allowsFloats = true
        return nf.number(from: contentText) != nil
    }

    override func didSelectPost() {
        // This is called after the user selects Post. Do the upload of contentText and/or NSExtensionContext attachments.
    
        // Inform the host that we're done, so it un-blocks its UI. Note: Alternatively you could call super's -didSelectPost, which will similarly complete the extension context.

        // This is safe to force-unwrap, because isContentValid checks that the contentText is a number
        let number = Double(contentText)!
        store(number: number)

        self.extensionContext!.completeRequest(returningItems: [], completionHandler: nil)
    }

    override func configurationItems() -> [Any]! {
        // To add configuration options via table cells at the bottom of the sheet, return an array of SLComposeSheetConfigurationItem here.
        return []
    }

}
