//
//  Utils.swift
//  NumberLibrary
//
//  Created by James Cash on 25-06-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

import Foundation

let numberKey = "201906_numberkey"
let defaultsSuite = "group.occasionallycogent.testing"

public func loadNumber() -> Double {
    return UserDefaults(suiteName: defaultsSuite)!.double(forKey: numberKey)
}

public func store(number: Double) {
    UserDefaults(suiteName: defaultsSuite)!.set(number, forKey: numberKey)
}
