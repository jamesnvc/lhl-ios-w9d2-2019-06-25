//
//  NumberLibrary.h
//  NumberLibrary
//
//  Created by James Cash on 25-06-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for NumberLibrary.
FOUNDATION_EXPORT double NumberLibraryVersionNumber;

//! Project version string for NumberLibrary.
FOUNDATION_EXPORT const unsigned char NumberLibraryVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <NumberLibrary/PublicHeader.h>


